## Diagramme service adresse

```mermaid
sequenceDiagram
client ->> APIM: appel fichier ou unitaire (Oauth)
Note right of APIM: authentification, <br>habilitation, quota
APIM-->>Adresse: 
Adresse -->>CPN: appel unitaire
Note right of CPN: (identification adresse et tournee)<br/>machine de tri,ocr,alias, racam, rao.
Adresse -->> Serca : appel unitaire 
Note right of Serca: Backup cpn si <br>ko ou pas de réponse.
Adresse -->> orgaTE: identifiant tournée
Note right of orgaTE: Orga Travaux Externes<br>récup info tournée<br> (libellé, type, site distri).
Adresse -->> RZP: identifiant adresse Ligne5-6
Note right of RZP: Réf. Zones Prestées<br>récupération id<br>site distri.
Adresse -->> source Orga: identifiant de site
Note right of source Orga: récup infos détaillées<br>(code roc, regate, libellé)
```
```mermaid
